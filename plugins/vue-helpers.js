import Vue from 'vue';

Vue.mixin({
	methods: {
		getMediaURL(url) {
			if (url.substr(0, 4) !== 'http') {
				return `${process.env.CMS_URL}${url}`;
			}

			return url;
		},
	},
});
