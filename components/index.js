import ComponentContentArticle from '~/components/ArticleCard.vue';
import ComponentContentDivider from '~/components/Divider.vue';
import ComponentContentTimer from '~/components/Timer.vue';
import ComponentContentVideo from '~/components/Video.vue';

export default {
	ComponentContentArticle,
	ComponentContentDivider,
	ComponentContentTimer,
	ComponentContentVideo,
};
