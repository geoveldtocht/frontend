module.exports = {
	apps: [
		{
			name: 'geoveldtocht-frontend',
			cwd: '/home/gitlab/geoveldtocht/frontend',
			script: 'npm',
			args: 'start',
			env: {
				NODE_ENV: 'production',
			},
		},
	],
};
