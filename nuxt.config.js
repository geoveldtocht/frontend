const ENV_FILE = process.env.NODE_ENV === 'production' ? '.env.prd' : '.env';

require('dotenv').config({ path: `${process.env.PWD}/${ENV_FILE}` });

const modules = [
	'@nuxtjs/pwa',
	// Doc: https://github.com/nuxt-community/dotenv-module
	'@nuxtjs/dotenv',
	'@nuxtjs/apollo',
];

if (process.env.NODE_ENV === 'production') {
	modules.push(['@nuxtjs/google-tag-manager', { id: 'GTM-KM96DCG' }]);
}

export default {
	mode: 'universal',

	server: {
		host: `${process.env.NUXT_HOST}`,
		port: `${process.env.NUXT_PORT}`,
	},

	/*
	 ** Headers of the page
	 */
	head: {
		title: process.env.npm_package_name || '',
		meta: [
			{ charset: 'utf-8' },
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1',
			},
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || '',
			},
		],
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: { color: '#fff' },
	/*
	 ** Global CSS
	 */
	css: ['~/assets/style/main.css'],
	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: ['~/plugins/vue-helpers.js'],
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [['@nuxtjs/dotenv', { filename: ENV_FILE }]],
	/*
	 ** Nuxt.js modules
	 */
	modules,
	/*
	 ** Build configuration
	 */
	build: {
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {},
	},
	apollo: {
		// cookieAttributes: {
		// 	/**
		// 	 * Define when the cookie will be removed. Value can be a Number
		// 	 * which will be interpreted as days from time of creation or a
		// 	 * Date instance. If omitted, the cookie becomes a session cookie.
		// 	 */
		// 	expires: 7, // optional, default: 7 (days)

		// 	/**
		// 	 * Define the path where the cookie is available. Defaults to '/'
		// 	 */
		// 	path: '/', // optional
		// 	/**
		// 	 * Define the domain where the cookie is available. Defaults to
		// 	 * the domain of the page where the cookie was created.
		// 	 */
		// 	domain: 'example.com', // optional

		// 	/**
		// 	 * A Boolean indicating if the cookie transmission requires a
		// 	 * secure protocol (https). Defaults to false.
		// 	 */
		// 	secure: false
		// },
		// optional
		watchLoading: '~/plugins/apollo-watch-loading-handler.js',
		// optional
		errorHandler: '~/plugins/apollo-error-handler.js',
		// required
		clientConfigs: {
			default: '~/apollo/config.js',
		},
	},
};
