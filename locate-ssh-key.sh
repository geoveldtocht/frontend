#!/bin/bash -ex

# Install ssh-agent if not already installed, it is required by Docker.
# (change apt-get to yum if you use a CentOS-based image)
which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )

# Run ssh-agent (inside the build environment)
eval $(ssh-agent -s)

mkdir -p ~/.ssh

# Add the SSH key stored in SSH_KEY variable to the agent store
echo "$SSH_KEY" > ~/.ssh/id_rsa
chmod 400 ~/.ssh/id_rsa
ssh-add ~/.ssh/id_rsa

# For Docker builds disable host key checking. Be aware that by adding that
# you are suspectible to man-in-the-middle attacks.
# WARNING: Use this only with the Docker executor, if you use it with shell
# you will overwrite your user's SSH config.
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config