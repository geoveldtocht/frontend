import cache from './cache';

export default function(ctx) {
	return {
		cache,

		// required
		httpEndpoint: `${process.env.CMS_URL}${process.env.CMS_GRAPHQL_URI}`,
		// optional
		// See https://www.apollographql.com/docs/link/links/http.html#options
		httpLinkOptions: {
			credentials: 'same-origin',
		},
		// You can use `wss` for secure connection (recommended in production)
		// Use `null` to disable subscriptions
		// wsEndpoint: 'ws://localhost:1337', // optional
		// LocalStorage token
		tokenName: 'apollo-token', // optional
		// Enable Automatic Query persisting with Apollo Engine
		persisting: false, // Optional
		// Use websockets for everything (no HTTP)
		// You need to pass a `wsEndpoint` for this to work
		websocketsOnly: false, // Optional
	};
}
